function Combat(player, enemy){
	this.combatants = [player, enemy];
	this.turn = [];
	this.currentTurn;
	this.enemy = enemy;
	this.player = player;
	
	this.setTurns();
}

Combat.prototype.setTurns = function(){
	this.turn = this.combatants.sort(function(a,b){
  	return b.spd - a.spd;
  })
}

Combat.prototype.nextTurn = function(){
	if (!this.currentTurn){
		this.currentTurn = this.turn.shift();
	} else {
		this.turn.push(this.currentTurn);
		this.currentTurn = this.turn.shift();
	}
}

Combat.prototype.playerTurn = function(){
	return this.currentTurn.playerCharacter;
}


/*
combat.prototype.findPlayer = function(){
	var a = this.combatants.filter(function(a){
		return a.playerCharacter;
	});
	return a;
}
combat.prototype.findNPC = function(){
	var a = this.combatants.filter(function(a){
		return !a.playerCharacter;
	});
	return a;
}
*/