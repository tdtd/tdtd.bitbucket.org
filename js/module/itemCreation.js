var slots = {
	weapon:		['Dagger', 'Knife', 'Cestus', 'Sickle', 'Club', 'Mace', 'Pike', 'Spear', 'Rapier', 'Short Sword', 'Gladius', 'Khukari', 'Falchion'],
	shield:		['Shield', 'Tower Shield', 'Spike Shield'],
	head:	 		['Helm', 'Gorget'],
	chest:		['Pauldron','Breastplate','Hauberk', 'Chain Mail'],
	hands:		['Gauntlets', 'Bracers'],
	pants:		['Greaves', 'Faulds'],
	boots: 		['Boots', 'Sandals']
}
	

// Weapon Types
var weaponTypes = {
	knife: ['Dagger', 'Knife', 'Sickle', 'Khukari'],
	fist: ['Cestus'],
	hammer: ['Club', 'Mace'],
	lance: ['Pike', 'Spear'],
	sword: ['Short Sword', 'Gladius', 'Falchion', 'Rapier']
}


function Item(itemObj){
	var rarityColor = ['white','blue','green','yellow','orange','red'];
	var rarities = ['Trash','Common','Uncommon','Rare','Antique','Legendary'];
	
	this.type = itemObj.type;
	this.slot = itemObj.slot;
	this.level = itemObj.level || 1;
	this.rarityNum = itemObj.rNum;
	this.rarity = rarities[itemObj.rNum];
	this.material = itemObj.material || 'Iron';
	
	this.weaponType = this.getWeaponType();
	
	this.stats = {
		str: itemObj.stats.str,
		agi: itemObj.stats.agi,
		int: itemObj.stats.int,
		chr: itemObj.stats.chr,
		wis: itemObj.stats.wis,
		con: itemObj.stats.con,
		
		spd: itemObj.stats.spd,
		atk: itemObj.stats.atk,
		def: itemObj.stats.def, 
		mag: itemObj.stats.mag,
		pur: itemObj.stats.pur,
		swy: itemObj.stats.swy
	}
	
	this.price = this.level * (this.rarityNum + 1)  * 10;
	this.color = rarityColor[this.rarityNum];
	this.name = itemObj.name || this.itemNameGen();
};

//Generate an item Name
Item.prototype.itemNameGen = function(){
	var fullName = '';
	fullName += this.material;
	fullName += ' '+this.type;
	
	return fullName;
};

Item.prototype.getWeaponType = function(){
	if (this.slot == 'weapon'){
		var a = Object.keys(weaponTypes);
		a.forEach(function(key){
			if (weaponTypes[key].indexOf(this.type) != -1){
				return key;
			}
		})
	}
}

//Generate a Random Item
function ranItem(level){
	var lvl = varyLvl(level, 2, -8);
	var s = ['weapon', 'shield', 'head', 'chest', 'hands', 'pants', 'boots'];
	var slot = s[ran(0, s.length - 1)];
	var slotArr = slots[slot];
	var type = slotArr[ran(0, slotArr.length - 1)];
	var rarRan = Math.floor(Math.pow( Math.random(), 2 )*6);	
	var stats = {
		str: 0,
		agi: 0,
		int: 0,
		wis: 0,
		chr: 0,
		con: 0,
		spd: 0,
		atk: 0,
		def: 0,
		mag: 0,
		pur: 0,
		swy: 0
	}
	
	for (var i = 0; i <= rarRan; i++){
		stats[randomProperty(stats)] += (.25*lvl);
	}
	Object.keys(stats).forEach(function(key){
		stats[key] = Math.floor(stats[key]);
	})
	
	return new Item({type: type, slot: slot, level: lvl, rNum: rarRan, stats: stats});
};

//var weaponName = new Foswig(3);
//var ownerName = new Foswig(4);
//weaponName.addWordsToChain(weaponArray);
//ownerName.addWordsToChain(nameArray);