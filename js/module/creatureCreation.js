/*
*
* Creature / Player Functions and Vars
*
*/

var jobs = {
	list: ['Warrior', 'Mage', 'Cleric', 'Rogue'],
	Warrior: new Job('Warrior',[3,0,1,2,1,2], {description: 'Masters of muscle and steel.', alignment: {good: .5, lawful: .7}}),
	Mage: new Job('Mage',[0,3,2,2,1,1], {description: 'Master of the Arcane and unknown.', alignment: {good: .5, lawful: .5}}),
	Cleric: new Job('Cleric', [1,2,3,2,0,1], {description: 'Follower of a holy order.', alignment: {good: .8, lawful: .5}}),
	Rogue: new Job('Rogue',[0,1,2,3,2,1], {description: 'Trickster Thief, lives outside the law.', alignment: {good: .5, lawful: .2}}),
	PigSticker: new Job('Pig Sticker',[1,0,0,2,0,1], {description: 'Not an advanced job, basically anyone with a knife and a dream.', alignment: {good: .5, lawful: .5}}),
}

var races = {
	list: ['Human', 'Elf', 'Dwarf'],
	Human: new Race('Human', [1,1,1,1,1,1], {description: 'Humanoid descended from monkeys. Hardy and versatile', height: 60, weight: 150}, ['brown', 'blonde', 'red', 'auburn', 'gray'], 'skin', ['pale', 'pink', 'tanned', 'dark', 'black'], 2, ['brown', 'green', 'blue', 'amber', 'gray']),
	Elf: new Race('Elf', [0,1,2,2,0,1], {description: 'The Fairest Race, live secluded in woods', height: 72, weight: 110}, ['blonde', 'red', 'auburn', 'gray', 'white', 'blue', 'purple'],'skin', ['pale blue', 'white', 'pale green', 'pale pink'], 2, ['brown', 'green', 'blue', 'amber', 'gray']),
	Dwarf: new Race('Dwarf', [2,0,1,0,1,2], {description: 'Stocky and short denizens of the mountains roots', height: 36, weight: 130}, ['brown', 'blonde', 'red', 'auburn', 'gray'],'skin', ['pale', 'pink', 'tanned', 'dark', 'black'], 2, ['brown', 'green', 'blue', 'amber', 'gray']),
	Orc: new Race('Orc', [4,0,0,2,0,4], {description: 'Large brutes, not known for civility', height: 72, weight: 260}, ['black','green','purple','blue'],'skin', ['green', 'yellow', 'brown', 'red'], 2, ['brown', 'green', 'blue', 'amber', 'gray']),
	Goblin: new Race('Goblin', [1,1,0,2,0,0], {description: 'Tiny balls of hatred and snot.', height: 24, weight: 50}, ['black','green','purple','blue'],'skin', ['green', 'yellow'], 2, ['brown', 'green', 'blue', 'amber', 'gray']),
	Golem: new Race('Golem', [5,5,5,5,5,5], {description: 'A Large construct that is part magic, part mechanic, and all destruction', height: 80, weight: 750}, [''], 'rock', ['igneous', 'slate', 'obsidian'], 2, ['black']),
	Ratkin: new Race('Ratkin', [0,2,0,2,0,1], {description: 'Something between a goblin and a rat. You shudder to think how that may have happened.', height: 20, weight: 25}, [''],'fur', ['black', 'gray', 'brown', 'mottled'], 2, ['black', 'red'])
}

var hair = {
	color: ['brown', 'blonde', 'red', 'auburn', 'gray'],
	
}

var eyes = {
	number : 2,
	color: ['brown', 'green', 'blue', 'amber', 'gray']
}

var skin = {
	type: ['skin'],
	color: ['pale', 'pink', 'tanned', 'dark', 'black', 'green']
}

var gender = ['Male', 'Female', 'Unknown']

var blemishes = {
		face: ['Right Eye Scar', 'Left Eye Scar', 'Left Eye Beauty Mark', 'Right Eye Beauty Mark', 'Left Eye Burn Mark', 'Right Eye Burn Mark', 'Left Eye Arcane Burn Mark', 'Right Eye Arcane Burn Mark'],
		torso: ['Chest Scar', 'Upper Chest Mole', 'Arcane Burn Mark', 'Burn Mark'],
		rightArm: ['Bicep Scar', 'Arcane Burn Mark', 'Burn Mark'],
		leftArm: ['Bicep Scar', 'Arcane Burn Mark', 'Burn Mark'],
		rightLeg: ['Thigh Scar', 'Arcane Burn Mark', 'Burn Mark'],
		leftLeg: ['Thigh Scar', 'Arcane Burn Mark', 'Burn Mark']
	};

/*
*
* Constructors
*
*/

//  ----- Job Obj
function Job(name, arr, obj){
	var stats = {str:0, int:0, wis:0, agi:0, chr:0, con:0};
	
	Object.keys(stats).forEach(function(key, ind){
		stats[key] = arr[ind];
	})
	this.description = obj.description || 'Unknown Job';
	this.alignment = obj.alignment || {good: .5, lawful: .5}; 
	this.name = name;
  this.stats = stats;
}

//  ----- Race
function Race(name, arr, desc, hairColor, skinType, skinColor, eyeNum, eyeColor){
	var stats = {str:0, int:0, wis:0, agi:0, chr:0, con:0};
	
	Object.keys(stats).forEach(function(key, ind){
		stats[key] = arr[ind];
	})
	this.skinColAvailable = skinColor|| 'Placeholder Description';
	this.hairColAvailable = hairColor|| ['black'];
	this.eyeColAvailable = eyeColor|| ['pale', 'pink', 'tanned', 'dark', 'black', 'green'];
	
	this.name = name;
  this.stats = stats;
	this.description = desc.description ;
	this.height = desc.height || 0;
	this.weight = desc.weight || 0;
	this.hairColors = this.hairColAvailable[ran(0, this.hairColAvailable.length)];
	this.skinType = skinType || 'skin';
	this.skinColor = this.skinColAvailable[ran(0, this.skinColAvailable.length)];
	this.length = 1;
}

//  ----- Creature Obj
function Creature (obj){
	this.name = obj.name || 'Jon';
	this.job = obj.job || jobs.Warrior;
	this.race = obj.race || races.Human;
	this.gender = obj.gender || 'Unknown';
	this.level = obj.level || 1;
	this.statPoints = 1;
	
	
	this.playerCharacter = false;
	this.preferedAction = 'attack';
	
	this.stats = obj.stats || {
		str: 0,
		agi: 0,
		int: 0,
		wis: 0,
		chr: 0,
		con: 0
	};
	
	this.health = obj.health || 10;
	this.maxHealth = this.health;
	this.mana = obj.mana || 10;
	this.maxMana = this.mana;
	this.stamina = obj.stamina || 10;
	this.maxStamina = this.stamina;
	
	this.swayed = 0;
	this.resolution = 50;
	this.conciousness = 0;
	
	this.experience = obj.exp || 0;
	this.totalExperience = 0;
	this.totalExperienceGained = 0;
	this.nextLevel = obj.nextLevel || 100; 
	
	this.gold = 0;
	this.magnicyte = 0;
	
	this.spd = obj.spd || 1;
	this.atk = obj.atk || 1;
	this.def = obj.def || 1;
	this.mag = obj.mag || 1;
	this.pur = obj.pur || 1;
	this.swy = obj.swy || 1;
	
	this.str = obj.str || 1;
	this.agi = obj.agi || 1;
	this.int = obj.int || 1;
	this.chr = obj.chr || 1;
	this.wis = obj.wis || 1;
	this.con = obj.con || 1;
	
	this.good = obj.good || .50;
	this.lawful = obj.lawful || .50;
	
	this.equipment = {
		weapon: null,
		shield: null,
		head: null,
		chest: null,
		hands: null,
		pants: null,
		boots: null,
	}
	
	this.equipmentStats = {
		str: 0,
		agi: 0,
		int: 0,
		wis: 0,
		chr: 0,
		con: 0,
		spd: 0,
		atk: 0,
		def: 0,
		mag: 0,
		pur: 0,
		swy: 0
	}
	
	this.eyes = {
		color:'blue',
		number: 2
	}
	
	this.hair = {
		color: 'brown',
		length: 1
	}
	
	this.skin = {
		type: 'skin',
		color: 'pale'
	}
	
	this.height = 60;
	
	this.weight = 150;
	
	this.blemishes = {
		face: [],
		torso: [],
		rightArm: [],
		leftArm: [],
		rightLeg: [],
		leftLeg: []
	};
	this.maxInventory = 6;
	this.inventory = [];
	this.maxCampChest = 30;
	this.campChest = [];
	
	this.keyItems = [];
	
	this.created = false; //Only for player Characters
	
	this.updateAll();
	this.rest();
}

Creature.prototype.updateAll = function(){
	this.updatePrim();
	this.updateHMS();
	this.updateSec();
}

Creature.prototype.updatePrim = function(){
	this.str = this.race.stats.str + this.job.stats.str + this.stats.str + this.equipmentStats.str;
	this.agi = this.race.stats.agi + this.job.stats.agi + this.stats.agi + this.equipmentStats.agi;
	this.int = this.race.stats.int + this.job.stats.int + this.stats.int + this.equipmentStats.int;
	this.wis = this.race.stats.wis + this.job.stats.wis + this.stats.wis + this.equipmentStats.wis;
	this.chr = this.race.stats.chr + this.job.stats.chr + this.stats.chr + this.equipmentStats.chr;
	this.con = this.race.stats.con + this.job.stats.con + this.stats.con + this.equipmentStats.con;
	console.log('updated all');
}

Creature.prototype.updateSec = function(){
	this.spd = calcSecondary(this.agi, this.chr) + this.equipmentStats.spd;
	this.atk = calcSecondary(this.str, this.agi) + this.equipmentStats.atk;
	this.def = calcSecondary(this.con, this.str) + this.equipmentStats.def;
	this.mag = calcSecondary(this.int, this.wis) + this.equipmentStats.mag;
	this.pur = calcSecondary(this.wis, this.con) + this.equipmentStats.pur;
	this.swy = calcSecondary(this.chr, this.int) + this.equipmentStats.swy;
}

Creature.prototype.updateHMS = function(){
	this.maxHealth = this.con * 5 + 10;
	this.maxMana = this.mag * 5 + 10
	this.maxStamina = Math.floor((this.agi * 2.5)+(this.str * 2.5)) + 10;
	this.resolution = this.calcRes();
}

Creature.prototype.setJob = function(jobName){
	this.job = jobs[jobName];
}
Creature.prototype.setRace = function(raceName){
	this.race = races[raceName];
}

Creature.prototype.describe = function(){
	var a = this.name+' stands before you. They are a(n) '+this.race.name+' ['+this.race.description+'] '+this.job.name+'. They have '+this.eyes.number+' '+this.eyes.color+' eyes and '+this.skin.color+' '+this.skin.type+'. Their '+this.hair.color+' hair is '+this.hair.length+' inches long.';
	return a;
}

Creature.prototype.rest = function(){
	this.health = this.maxHealth;
	this.mana = this.maxMana;
	this.stamina = this.maxStamina;
	this.swayed = 0;
	this.conciousness = 0;
}

Creature.prototype.rS = function(){
	var a = {str:0,agi:0,int:0,chr:0,wis:0,con:0}
	return { 
		str: this.str,
		int: this.int,
		agi: this.agi,
		wis: this.wis,
		chr: this.chr,
		con: this.con
	}
}

Creature.prototype.takeDamage = function(damage, type){
	var a = Math.floor(damage - (this.calcDef() * damage));
	if (a <= 0){
		a = 0;
	}
	var b = calcToZero(this.health, a)
	this.health = b.remain;
	return a;
}

Creature.prototype.takeSway = function(damage){
	var a = Math.floor((1 - (this.conciousness / 100)) * damage);
	console.log(a);
	return this.swayed += a;
}

Creature.prototype.takeBlunt = function(damage){
	return this.conciousness += damage;
}

Creature.prototype.dealDamage = function(enemy){
	if (enemy){
		var a = Math.floor(this.spd / enemy.spd);
		(a < 1) ? a = 1 : a;
		return {times: a, dmg: this.atk*a};
	}
	return {times: 1, dmg: this.atk};
}

Creature.prototype.dealSway = function(){
	return this.swy;
}

Creature.prototype.dealBlunt = function(){
	return (this.str + this.con) * 2;
}

Creature.prototype.calcDef = function (){
	var a = ((1)+(Math.floor(Math.log(this.def)+this.def*.75)))/100;
	return a;
}

Creature.prototype.calcRes = function (){
	var a = this.pur + this.swy,
			b = ((1)+(Math.floor(Math.log(a)+a*.75)))/100,
      c = Math.floor(a/25)+1,
      d = Math.floor((a%25)/2)
	return (c*25)+d;
}

Creature.prototype.attemptFlee = function(enemy){
	var a = this.spd;
	var b = enemy.spd;
	var f = (((a*128)/b)+30)%256;
	var r = ran(0,255);
	if (f > r){
		return true;
	}
	return false;
}

Creature.prototype.useMana = function(cost){
var a = calcToZero(this.mana, cost);
	this.mana = a.remain;
}

Creature.prototype.useStamina = function(cost){
	var a = calcToZero(this.stamina, cost);
	this.stamina = a.remain;
}

Creature.prototype.checkDead = function(){
	if (this.health <= 0){
		return true;
	}
	return false;
}

Creature.prototype.checkSway = function(){
	if (this.swayed >= this.resolution){
		return true;
	}
	return false;
}

Creature.prototype.checkUnconcious = function(){
	if (this.conciousness >= 100){
		return true;
	}
	return false;
}

Creature.prototype.sanityCheck = function(){
	if (this.health > this.maxHealth){
		this.health = this.maxHealth;
	}
	if (this.mana > this.maxMana){
		this.mana = this.maxMana;
	}
	if (this.stamina > this.maxStamina){
		this.stamina = this.maxStamina;
	}
}

Creature.prototype.expProvided = function(){
	return 5;
}

Creature.prototype.expGain = function(exp){
	this.experience += exp;
	this.checkLevel();
}

Creature.prototype.loseLevel = function(){
	this.experience = 0;
	this.updateAll();
}

Creature.prototype.checkLevel = function(){
	if (this.experience >= this.nextLevel){
		this.experience -= this.nextLevel;
		this.levelUp();
	}
}

Creature.prototype.levelUp = function(){
	this.level++;
	this.statPoints += 3;
	this.rest();
	this.nextLevel = Math.floor(this.level+(100*(Math.pow(2, this.level/8))));
}

Creature.prototype.statUp = function(stat){
	if (this.statPoints > 0 && !(this.stats[stat] + 1 > 99)){
		this.statPoints--;
		this.stats[stat]++;
	}
	this.updatePrim();
	this.updateHMS();
	this.updateSec();
}

Creature.prototype.getPreferedAction = function(){
	var arr = [{name: 'atk', int: this.dealDamage()*2}, {name:'swy', int: this.dealSway()}, {name: 'club', int: (this.dealBlunt()*.8)}];
	var a = arr.sort(function(a,b){
		return b.int - a.int;
	});
	return a[0].name;
}

Creature.prototype.setArmorStats = function(){
	var obj = {}
	//var st = ['str','agi','int','wis','chr','con','spd','atk','def','mag','pur','swy'];
	for (var item in this.equipment){
		if (this.equipment[item] != null){
			for (var stat in this.equipment[item].stats){		
				if (!obj[stat]){
					obj[stat] = this.equipment[item].stats[stat];
				} else {
					obj[stat] += this.equipment[item].stats[stat];
				}
			}	
		}	
	}
	_.merge(this.equipmentStats, obj);
	this.updateAll();
}

Creature.prototype.addItemToInventory = function(item){
	if (this.inventory.length < this.maxInventory){
		this.inventory.push(item);
		return {worked: true}
	} else {
		return {worked: false};
	}
}

Creature.prototype.removeItemFromInventory = function(index){
	var a = this.inventory.splice(index, 1);
	return a[0];
}

Creature.prototype.addItemToChest = function(item){
	this.campChest.push(item);
}

Creature.prototype.equipFromInventory = function(slot, index){
	var holdItem;
	var arr = Object.keys(this.equipment);
	
	if (arr.indexOf(slot) != -1){
		if (this.equipment[slot]){
			holdItem = this.equipment[slot];
			this.equipment[slot] = this.inventory[index];
			this.inventory[index] = holdItem;
		} else {
			this.equipment[slot] = this.inventory[index];
			this.inventory.splice(index, 1);
		}
	}
	this.setArmorStats();
	this.sanityCheck();
}

Creature.prototype.unequip = function(key){
	this.inventory.push(this.equipment[key]);
	this.equipment[key] = null;
}

Creature.prototype.removeGold = function(cost){
	if (this.gold - cost >= 0){
		this.gold -= cost;
		return {worked: true};
	} else {
		return {worked: false};
	}
}

Creature.prototype.changeGood = function(num){
	var a = num/100;
	if (num < 0){
		if (this.good + a > 0){
			this.good += a
		} else {
			this.good = 0;
		}
	} else {
		if (this.good + a < 1){
			this.good += a;
		} else {
			this.good = 1;
		}
	}
}
Creature.prototype.changeLawful = function(num){
	var a = num/100;
	if (num < 0){
		if (this.lawful + a > 0){
			this.lawful += a
		} else {
			this.lawful = 0;
		}
	} else {
		if (this.lawful + a < 1){
			this.lawful += a;
		} else {
			this.lawful = 1;
		}
	}
}
/*
*
* Generation Functions
*
*/


function generateCreature (level){
	if (level <= 6){
		var max = 1;
	} else if (level <= 12){
		var max = 2;
	} else if (level <= 18){
		var max = 3;
	} else if (level <= 24){
		var max = 4;
	} else {
		var max = 4;
	}
	
	var difficulty = 1;//ran(1,5);
	
	switch (difficulty){
		case 1:
			return genVEasyCreature(level);
			break;
		case 2:
			return genEasyCreature(level);
			break;
		case 3:
			return genMedCreature(level);
			break;
		case 4:
			return genHardCreature(level);
			break;
		case 5:
			return genVHardCreature(level);
			break;
	}
}


function genVEasyCreature(level){
	var smSt = {
		str: 0,
		agi: 0,
		int: 0,
		wis: 0,
		chr: 0,
		con: 0};
	var mdSt = {
		str: -1,
		agi: -1,
		int: -1,
		wis: -1,
		chr: -1,
		con: -1
	};
	var lgSt = {
		str: -2,
		agi: -2,
		int: -2,
		wis: -2,
		chr: -2,
		con: -2
	};
	
	var enemies = [
		{name: 'Gobby', race: races.Goblin, job: jobs.PigSticker, level: zero(level-2), stats: smSt},
		{name: 'Lar', race: races.Ratkin, job: jobs.Rogue, level: zero(level-2), stats: smSt},
		{name: 'Rema', race: races.Dwarf, job: jobs.Mage, level: zero(level-2), stats: mdSt},
		{name: 'Orag', race: races.Orc, job: jobs.Mage, level: zero(level-3), stats: mdSt}
	]
	return new Creature(enemies[ran(0, enemies.length-1)]);
}

function genEasyCreature(level){
	var smSt = {
		str: 1,
		agi: 1,
		int: 1,
		wis: 1,
		chr: 1,
		con: 1};
	var mdSt = {
		str: 0,
		agi: 0,
		int: 0,
		wis: 0,
		chr: 0,
		con: 0
	};
	var lgSt = {
		str: -1,
		agi: -1,
		int: -1,
		wis: -1,
		chr: -1,
		con: -1
	};
	
	var enemies = [
		{name: 'Grobo', race: races.Goblin, job: jobs.PigSticker, level: zero(level-1), stats: smSt},
		{name: 'Perry', race: races.Ratkin, job: jobs.Rogue, level: zero(level-1), stats: smSt},
		{name: 'Hrothgar', race: races.Human, job: jobs.Cleric, level: zero(level-1), stats: mdSt, gender: 'male'},
		{name: 'Zesae', race: races.Elf, job: jobs.Warrior, level: zero(level-1), stats: mdSt, gender: 'female'},
		{name: 'Hurf', race: races.Dwarf, job: jobs.Cleric, level: zero(level-1), stats: mdSt},
		{name: 'Orma', race: races.Orc, job: jobs.Rogue, level: zero(level-2), stats: mdSt, gender: 'female'}
	]
	return new Creature(enemies[ran(0, enemies.length-1)]);
}

function genMedCreature(level){
	var smSt = {
		str: 2,
		agi: 2,
		int: 2,
		wis: 2,
		chr: 2,
		con: 2};
	var mdSt = {
		str: 1,
		agi: 1,
		int: 1,
		wis: 1,
		chr: 1,
		con: 1
	};
	var lgSt = {
		str: 0,
		agi: 0,
		int: 0,
		wis: 0,
		chr: 0,
		con: 0
	};
	
	var enemies = [
		{name: 'Grobo', race: races.Goblin, job: jobs.PigSticker, level: zero(level), stats: smSt},
		{name: 'Perry', race: races.Ratkin, job: jobs.Rogue, level: zero(level), stats: smSt},
		{name: 'Hrothgar', race: races.Human, job: jobs.Cleric, level: zero(level), stats: mdSt, gender: 'male'},
		{name: 'Zesae', race: races.Elf, job: jobs.Warrior, level: zero(level), stats: mdSt, gender: 'female'},
		{name: 'Hurf', race: races.Dwarf, job: jobs.Cleric, level: zero(level), stats: mdSt},
		{name: 'Orma', race: races.Orc, job: jobs.Rogue, level: zero(level-1), stats: mdSt, gender: 'female'}
	]
	return new Creature(enemies[ran(0, enemies.length-1)]);
}

function genHardCreature(level){
		var smSt = {
		str: 3,
		agi: 3,
		int: 3,
		wis: 3,
		chr: 3,
		con: 3};
	var mdSt = {
		str: 2,
		agi: 2,
		int: 2,
		wis: 2,
		chr: 2,
		con: 2
	};
	var lgSt = {
		str: 1,
		agi: 1,
		int: 1,
		wis: 1,
		chr: 1,
		con: 1
	};
	
	var enemies = [
		{name: 'Grobo', race: races.Goblin, job: jobs.Warriorr, level: zero(level+2), stats: smSt},
		{name: 'Perry', race: races.Ratkin, job: jobs.Rogue, level: zero(level+2), stats: smSt},
		{name: 'Hrothgar', race: races.Human, job: jobs.Cleric, level: zero(level+2), stats: mdSt, gender: 'male'},
		{name: 'Zesae', race: races.Elf, job: jobs.Mage, level: zero(level+2), stats: mdSt, gender: 'female'},
		{name: 'Hurf', race: races.Dwarf, job: jobs.Warrior, level: zero(level+2), stats: mdSt},
		{name: 'Orma', race: races.Orc, job: jobs.Warrior, level: zero(level+2), stats: mdSt, gender: 'female'},
		{name: 'GOLEM #1002', race: races.Golem, job: jobs.Warrior, level: zero(level-1), stats: lgSt, gender: 'female'}
	]
	return new Creature(enemies[ran(0, enemies.length-1)]);
}
function genVHardCreature(){}