/*
*
* Chest Generation
*
*/
function Chest(lvl){
	this.closed = true;
	this.chest = 'Oak Chest';
	this.level = lvl;
	this.contents = this.generateContent();
}

Chest.prototype.generateContent = function(){
	if (ran(0,1)){
		return {gold:1 + Math.floor(this.level * ran(75, 125)/100)};
	} else{
		return {item: ranItem(this.level)};
	}
}
/*
*
* Shrine Generation
*
*/

function Shrine(lvl){
	this.closed = true;
	this.name;
	this.level = lvl;
	this.blessing = 0;
	this.contents = this.generateContent();
}

Shrine.prototype.generateContent = function(){
	if (ran(0,1)){
		this.blessing = 5;
		this.name = "Eternal Shrine";
	} else {
		this.blessing = -5;
		this.name = "Infernal Shrine";
	}
	return 1 + Math.floor(this.level * ran(200, 275)/100);
}

/*
*
* Wanderer Generation
*
*/
function Wanderer(lvl){
	this.name = 'wanderer'
	this.closed = true;
	this.chest = '';
	this.level = lvl;
	this.gold;
	this.item;
	
	this.generateContent();
}

Wanderer.prototype.generateContent = function(){
	this.gold = 1 + Math.floor(this.level * ran(10, 1250)/100);
	this.item = ranItem(1 + Math.floor(this.level * ran(10, 200)/100));
}

/*
*
* MapTiles
*
*/

function MapTile (obj, clss, collision, x, y, level){
	var tileType = obj.tileType || ['Sand', 'Rock', 'Cut Stone', 'Grass'];
	
	var visible = false;
	
	this.type = tileType[ran(0, tileType.length - 1)];
	this.level = level;
	this.class = 'w';
	this.special = '';
	this.collide = collision || 'true';
	this.room = clss || 'r';
	this.chest = this.generateChest(5);
	this.shrine = this.generateShrine(5);
	this.wanderer = this.generateWanderer(5);
	this.floorItems = [];
	this.description = this.generateDescription();
	this.style = {"box-shadow":""};
	this.x = x;
	this.y = y;
}

MapTile.prototype.generateChest = function(chance){
	var a = ran(1, 100);
	if (a <= chance){
		return new Chest(this.level);
	}
	return {closed: false};
}

MapTile.prototype.generateShrine = function(chance){
	var a = ran(1, 100);
	if (a <= chance && !this.chest.closed){
		return new Shrine(this.level);
	}
	return {closed: false};
}

MapTile.prototype.generateWanderer = function(chance){
	var a = ran(1, 100);
	if (a <= chance && !this.chest.closed && !this.shrine.closed){
		return new Wanderer(this.level);
	}
	return {closed: false};
}

MapTile.prototype.getChest = function(){
	return this.chest;
}

MapTile.prototype.openChest = function(){
	this.chest.closed = false;
	if (this.chest.contents.gold >= 0){
		return {message:{text:'You have looted a sack of gold from the chest, it contains '+this.chest.contents.gold+' gold.', class:''}, gold: this.chest.contents.gold};
	} else if (this.chest.contents.item){
		return {message:{text:'You have looted a '+this.chest.contents.item.rarity+' '+ this.chest.contents.item.type+' from the chest.', class:''}, item: this.chest.contents.item};
	} else {
		return {message: 'FUCKED UP'}
	}
}

MapTile.prototype.visitShrine = function(){
	this.shrine.closed = false;
}

MapTile.prototype.visitWanderer = function(){
	this.wanderer.closed = false;
}

MapTile.prototype.generateDescription = function(){
	var floor = this.type;
	var description = 'You have entered a  new room. The floor seems to be covered in '+floor+'.';
	
	return description;
}

MapTile.prototype.generateSpecial = function(){
	var floor = this.type;
	switch(this.special){
		case 'start':
			this.description = 'You are at the entrance of this floor of the dungeon. There does not appear to be a way to return.'; break;
		case 'end':
			this.description = 'You have found a the exit for the dungeon. There are two runes in front of you. You may take one to return to the surface or the other to continue deeper.'; break;
		default:
			return 'You have entered a  new room. The floor seems to be covered in '+floor+'.'; break;
	}
}

MapTile.prototype.checkWalls = function(){
	var obj = {
		text: 'The walls are made of stone and damp with moss. '+this.type,
	};
	return obj;
}

MapTile.prototype.getRoom = function(){
	return this.room;
}

/*
*
*     Button Generation to be displayed on Maze Button Bar
*			
*/
function MapButton(text, clss, fn){
	this.text = text;
	this.class = clss;
	this.fn = fn;
}

/*
*
*
*     Map Generation Code
*
*/

//Generate Blank 2 dimensional array of x length and y height
function Map (x,y, level){
	this.height = y,
	this.width = x;
	this.level = level;
	this.map = [];
	
	for(var i = 0; i < this.width; i++){
		var arr = [];
		for (var j = 0; j < this.height; j++){
			arr.push(new MapTile({}, 'wall', false, i, j, this.level));
		}
		this.map.push(arr);
	}
	
	console.log(this.level);
}

Map.prototype.localArea = function(x,y){
	var mapArr = [];
	for (var i = y-5; i < y+5; i++){
		var yArr = []
		if (!this.map[i]){
			this.map[i] = [];
		}
		for (var j = x-5; j < x+5; j++){
			if (this.map[i][j]){ 
				yArr.push(this.map[i][j]);
			} else {
				yArr.push(new MapTile({}, 'wall', false));
			}
		}
		mapArr.push(yArr);
	}
	return mapArr;
}

//Returns An Obj Containing the Map Array, A list of the entrance and exist
Map.prototype.fillMap = function(){
	var height = this.height-2,
			width = this.width-2,
			rooms = Math.round((this.height*this.width)/2),
			chestRoom = ran(2, rooms-2),
			startx = ran(1,height-2),
			starty = ran(1,width-2),
			curx = startx,
			cury = starty,
			aa = 0;
			
	this.map[startx][starty] = new MapTile({}, 's', 'false', startx, starty, this.level);
	
	while(rooms >= 0){
		var dir = ran(0,3),
				a = curx, //delta x
				b = cury; //delta y
		switch(dir){
			case 0:
				a -= 1;
				break;
			case 1:
			  b -= 1;
				break;
			case 2:
				a += 1;
				break;
			case 3:
			  b += 1;
				break;
		}
		if ((a > 0 && a < width ) && (b > 0 && b < height )){
			if (this.map[a][b].class != 'r' && this.map[a][b].class != 's'){
				
				if (rooms == chestRoom){
					this.map[a][b] = new MapTile({}, 'r', 'false', a, b, this.level); //Chest
				} else {
					this.map[a][b] = new MapTile({}, 'r', 'false', a, b, this.level); //Not Chest
				}
				
				curx = a;
				cury = b;
				rooms --;
			} else{
				curx = a;
				cury = b;
			}
		}
	}	
	this.map[curx][cury].room = 'e';
	this.map[curx][cury].special = 'end';
	this.map[curx][cury].generateSpecial();
	this.map[startx][starty].special = 'start';
	this.map[startx][starty].generateSpecial();

	this.entrance = [startx, starty];
	this.exit = [curx, cury];
	this.setClasses()
}

Map.prototype.refreshStates = function(){
	for (var i = 0; i < this.height; i++){
		for (var j = 0; j < this.width; j++){
			switch(this.map[i][j].room){
				case 'r':
					this.map[i][j].collide = false;
					this.map[i][j].special = '';
					this.map[i][j].generateSpecial();
					
					break;
				case 'wall':
					this.map[i][j].collide = true;
					this.map[i][j].special = '';
					this.map[i][j].generateSpecial();
					break;
				case 's':
					this.map[i][j].collide = false;
					this.map[i][j].special = 'start'
					this.map[i][j].generateSpecial();
					this.entrance = [i,j];
					break;
				case 'e':
					this.map[i][j].collide = false;
					this.map[i][j].special = 'end'
					this.map[i][j].generateSpecial();
					this.exit = [i,j];
					break;
				default:
					this.map[i][j].collide = true;
					this.map[i][j].special = '';
					this.map[i][j].generateSpecial();
					break;
			}
		}
	}
}

Map.prototype.setClasses = function(){
	var outlineColor = '#43545B';
	for (var i = 1, iLen = this.height-1; i < iLen; i++){
		for (var j = 1, jLen = this.width-1; j < jLen; j++){
			if (this.map[i][j].collide == 'true'){
				var a = this.map[i-1][j], //Top
						b = this.map[i+1][j], //bottom
						c = this.map[i][j-1], //left
						d = this.map[i][j+1]; //right
				this.map[i][j].style["background-color"] = '#2a363b';
				//Top
				if(a.collide == 'false'){
					this.map[i][j].style["box-shadow"] += '0 -1px 2px -1px black';
					this.map[i][j].style["border-top"] = '1px solid '+outlineColor;
				}
				//Bottom
				if(b.collide == 'false'){					
					if (this.map[i][j].style["box-shadow"].length > 1){
						this.map[i][j].style["box-shadow"] += ', '
					}
					
					this.map[i][j].style["box-shadow"] += '0 1px 2px -1px black';
					this.map[i][j].style["border-bottom"] = '1px solid '+outlineColor;
				}
				//Left
				if(c.collide == 'false'){
					if (this.map[i][j].style["box-shadow"].length > 1){
						this.map[i][j].style["box-shadow"] += ', '
					}
					this.map[i][j].style["box-shadow"] += '-1px 0 2px -1px black';
					this.map[i][j].style["border-left"] = '1px solid '+outlineColor;
				}
				//Right
				if(d.collide == 'false'){
					if (this.map[i][j].style["box-shadow"].length > 1){
						this.map[i][j].style["box-shadow"] += ', '
					}
					this.map[i][j].style["box-shadow"] += '1px 0 2px -1px black';
					this.map[i][j].style["border-right"] = '1px solid '+outlineColor;
				}
			}
		}
	}
}
