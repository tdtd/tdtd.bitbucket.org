function Shop(){
	Creature.call(this, name);
	this.shopInventory = [];
	this.gold = this.setGold();
	this.shopUpgradeLevel = 0;
	this.playerLevel = 1;
	this.rebuy = .2;
	this.setNewInventory();
}

extend(Creature, Shop);

Shop.prototype.setGold = function(){
	return 200+ran(10, 300);
}

Shop.prototype.setNewInventory = function(level){
	var a = ran(3,5);
	if (level){
		this.playerLevel = level;
	}
	this.shopInventory = [];
	for (var i = 0; i < a; i++){
		this.shopInventory.push(ranItem(this.shopUpgradeLevel + this.playerLevel));
	}
}

Shop.prototype.upgradeCost = function(){
	return 100 + (this.shopUpgradeLevel * 75);
}

Shop.prototype.refreshGold = function(){
	var a = this.setGold();
	this.gold = a + (a * this.shopUpgradeLevel);
}

Shop.prototype.upgrade = function(money){
	if (money > this.upgradeCost()){
		this.shopUpgradeLevel++;
		this.setNewInventory();
		
		return {worked: true};
	} else {
		return {worked: false}
	}
}