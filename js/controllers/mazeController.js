app.controller('MazeCtrl', function($scope, $location, $timeout, playerService, buttonService, mapService, timeService, lockout){
	var chance = 20,
			chest,
			desc = new Describe();
	
	$scope.name = 'Maze Controller';	
	$scope.maps = mapService;
	$scope.player = playerService.player;
	$scope.messages = [{
		text: 'You enter the dungeon. There is a musty smell in the air. It is somewhat foreboding yet simultaneously comforting. Danger feels right.',
		'class': 'entrance'
	}];
	$scope.buttons = [];
	$scope.combatActive = false;
	$scope.enemy;
	$scope.combat;
	$scope.turn;
	
	
	//Necessary Public Vars
	$scope.curLoc = function(){
		return mapService.currentLoc;	
	};
	$scope.inventory = function(){
		return playerService.player.inventory;
	}
	$scope.floorItems = function(){
		return info().floorItems;
	}
	//Watch for moved to be broadcast from the base controller
	$scope.$on('moved', function (event, data) {
		var a = spawnMonster(chance);
		if (a){
			startCombat();
		} else {
			timeService.incrementMinutes(1);
			handleRoom();
		}
	});
	
	//Watch for $scope.turn to change
	$scope.$watch('turn', function(newVal, oldVal){
		if (newVal){
			if(!newVal.playerCharacter){
				handleEnemy($scope.enemy.getPreferedAction());
			} else {

			}
		}
	})
	
	//Info for Current Map Tile
	function info(){
		if (mapService.mapObj){
			var a = mapService.mapObj.map;
			if (a){
				return a[$scope.curLoc()[0]][$scope.curLoc()[1]];
			};
		}
	};

	/*
	*
	* Generate Messages and send them to the Messages array
	*
	*/
	
	function Describe(){};
	
	Describe.prototype.myHealth = function(){
		generateMessage({text: convertHealth($scope.combat.player.health, $scope.combat.player.maxHealth), 'class': 'health'})
	};
	Describe.prototype.enemHealth = function(){
		generateMessage({text: convertHealth($scope.combat.enemy.health, $scope.combat.enemy.maxHealth), 'class': 'health'})
	};
	Describe.prototype.enemMana = function(){
		generateMessage({text: convertHealth($scope.combat.enemy.mana, $scope.combat.enemy.maxMana), 'class': 'mana'})
	};
	Describe.prototype.enemEquipment = function(){
		generateMessage({text: $scope.combat.enemy.equipment})
	};
	Describe.prototype.description = function(){
		generateMessage({text: $scope.combat.enemy.describe()})
	};
	Describe.prototype.walls = function(){
		generateMessage({text: info().checkWalls().text})
	}
	
	function handleRoom(){
		if (info().special == 'end'){
			clearButtons();
			clearCurrentRoom();
			mapService.mapObj = null;
			mapService.minimap = null;
			playerService.player.expGain(50);
			lockout.travel = false;
			goto('/camp');
		}
		
		//chest = info().chest;
		generateMessage();
		generateButtons();
	};
	
	function clearCurrentRoom(){
		$scope.enemy = null;
		$scope.turn = null;
		$scope.combat = null;
	}
	
	//
	function generateMessage(obj){
		if (obj){
			$scope.messages.push(obj);
		} else {
			var obj = {};
			obj.text = info().description;
			if (info().chest.closed){
				obj.class = "chest";
				obj.text += " There is a chest on the floor!!"
			}
			if (info().shrine.closed){
				obj.class = "chest";
				obj.text += "You have stumbled up an "+info().shrine.name+". What will you do?"
			}
			if (info().wanderer.closed){
				obj.class = "chest";
				obj.text += "You have stumbled upon a wounded wanderer. What will you do?"
			}
			$scope.messages.push(obj);
		}
	}
	
	function generateCombatMessage(){
		var obj = {};
		obj.text = 'You have been ambushed by '+$scope.combat.enemy.name;
		obj.class = 'enemy'
		generateMessage(obj);
	}
	
	/*
	*
	* Handle Buttons for $scope.buttons array
	*
	*/
	
	function clearButtons(){
		$scope.buttons = [];
	}
	
	function generateButtons(){
		clearButtons();
		
		if (info().chest.closed){
			$scope.buttons.push(new MapButton('Chest', 'btn btn-warning', openChest));
		}
		
		if (info().shrine.closed){
			$scope.buttons.push(new MapButton('Pray', 'btn btn-info', prayShrine));
			$scope.buttons.push(new MapButton('Desecrate', 'btn btn-danger', desecrateShrine));
		}
		
		if (info().wanderer.closed){
			$scope.buttons.push(new MapButton('Help', 'btn btn-info', helpWanderer));
			$scope.buttons.push(new MapButton('Kill', 'btn btn-danger', killWanderer));
		}
		$scope.buttons.push(new MapButton('Inspect Walls', 'btn btn-success', desc.walls));
	}
	
	function combatButtons(){
		clearButtons();
		actionButtons();
		inspectionButtons();
	}
	
	function actionButtons(){
		$scope.buttons.push(new MapButton('Attack', 'btn btn-danger', attack));
		$scope.buttons.push(new MapButton('Sway', 'btn btn-danger', sway));
		$scope.buttons.push(new MapButton('Club', 'btn btn-danger', club));
		$scope.buttons.push(new MapButton('Flee', 'btn btn-danger', flee));
	}
	
	function inspectionButtons(){
		var inspect = [];
		var variables = [{text: 'Health', function: 'enemHealth' }, {text: 'Mana', function: 'enemMana' }, {text: 'Equipment', function: 'enemEquipment' }, {text: 'Description', function: 'description' }];
		
		variables.forEach(function(v){
			$scope.buttons.push(new MapButton(v.text, 'btn btn-primary', desc[v.function]));
		});
	}
	
	
	// Handle Win State Functions (Club / Sway)
	function clubVictory(){
		clearButtons();
		$scope.buttons.push(new MapButton('Arrest', 'btn btn-danger', arrest));
		$scope.buttons.push(new MapButton('Coup de Grace', 'btn btn-danger', coup));
		$scope.buttons.push(new MapButton('Leave', 'btn btn-danger', leave));
		$scope.buttons.push(new MapButton('Pick Pocket', 'btn btn-danger', pickpocket));
	}
	
	/*
	*
	* Handle map items
	*
	*/
	//Front Facing Buttons
	$scope.pickup = function(index){
		var	x = info().x,
				y = info().y,
				a = mapService.mapObj.map[x][y].floorItems.splice(index, 1),
				item = a[0];
		if (a){
			if (!playerService.player.addItemToInventory(item).worked){
				console.log('pickup failed')
				mapService.mapObj.map[x][y].floorItems.push(item);
				generateMessage({text:'You could not pick up the '+item.name, class:''});
			} else {
				generateMessage({text:'You picked up the '+item.name, class:''});
			}
		}
	}
	
	$scope.drop = function(index){
		var x = info().x,
				y = info().y;
		if (playerService.player.inventory.length > 0){
			var a = playerService.player.removeItemFromInventory(index);
			mapService.mapObj.map[x][y].floorItems.push(a);
			generateMessage({text:'You dropped the '+a.name+'.', class:''});
		}
	}
	
	/*
	*
	*  Random Map Events
	*
	*/
	
	//Chest
	function openChest(){
		var a = info().openChest();
		if (a.gold){
			generateMessage(a.message);
			playerService.player.gold += a.gold
		} else if (a.item){
			generateMessage(a.message);
			if(!playerService.player.addItemToInventory(a.item).worked){
				generateMessage({text: 'You are carrying too much loot. The '+a.item.name+' falls to the ground', class:''})
				mapService.mapObj.map[info().x][info().y].floorItems.push(a.item);
			}
		}
		//chest = null;
		generateButtons();
	}
	
	//Shrine
	function prayShrine(){
		generateMessage({text: 'You prayed to the '+info().shrine.name+'. You receive its blessings.', class:''});
		playerService.player.changeGood(info().shrine.blessing);
		playerService.player.rest();
		info().visitShrine();
		generateButtons();
	}
	function desecrateShrine(){
		generateMessage({text: 'You destroy the '+info().shrine.name+'. While desecrating the holy site you find offerings left to it to the tune of '+info().shrine.contents+' gold!', class:''});
		playerService.player.gold += info().shrine.contents;
		info().visitShrine();
		generateButtons();
	}
	
	//Wanderer
	function helpWanderer(){
		generateMessage({text: 'You give some extra provisions to the '+info().wanderer.name+' and point them towards town. They reward you with gold', class:''});
		playerService.player.gold += info().wanderer.gold;
		playerService.player.changeLawful(5);
		info().visitWanderer();
		generateButtons();
	}
	function killWanderer(){
		generateMessage({text: 'You slaughter the '+info().wanderer.name+'. You loot the '+info().wanderer.item.name+' from their corpse.', class:''});
		if(!playerService.player.addItemToInventory(info().wanderer.item).worked){
				generateMessage({text: 'You are carrying too much loot. The '+info().wanderer.item.name+' falls to the ground', class:''})
				mapService.mapObj.map[info().x][info().y].floorItems.push(info().wanderer.item);
			}
		playerService.player.changeLawful(-5);
		info().visitWanderer();
		generateButtons();
	}
	/*
	*
	* Handle Combat
	*
	*/
	
	//Function for chance to spawn a monster
	function spawnMonster(chance){
		var a = ran(1,100);
		if (a <= chance && !lockout.maze){
			return true;
		}
		return false;
	}
	//Function to get the field ready for combat
	function startCombat(){
		lockout.maze = true;
		$scope.combatActive = true;
		$scope.enemy = generateCreature(playerService.player.level);
		$scope.combat = new Combat(playerService.player, $scope.enemy);
		$scope.combat.nextTurn();
		$scope.turn = $scope.combat.currentTurn;
		generateCombatMessage();
		combatButtons();
	}
	
	function handleCombat(){
		if ($scope.enemy.checkDead()){
			playerService.player.expGain($scope.enemy.expProvided());
			generateMessage({text: 'You are victorious.'});
			lockout.maze = false;
			handleRoom();
			clearCurrentRoom();
			timeService.incrementMinutes(5);
		} else if ($scope.enemy.checkUnconcious()){
			playerService.player.expGain($scope.enemy.expProvided());
			generateMessage({text: 'You knocked the enemy unconcious.'});
			clubVictory();
			
		} else if ($scope.enemy.checkSway()){
			playerService.player.expGain($scope.enemy.expProvided());
			generateMessage({text: 'You have swayed the enemy.'});
			lockout.maze = false;
			handleRoom();
			clearCurrentRoom();
			timeService.incrementMinutes(5);
		}
		
		if ($scope.player.checkDead()){
			generateMessage({text: 'You died. Reseting World. Try not to do that again.'});
			clearButtons();
			clearCurrentRoom();
			$timeout(function(){
				lockout.maze = false;
				lockout.travel = false;
				mapService.mapObj = {};
				playerService.player.loseLevel();
				timeService.forceEnd();
				playerService.player.rest();
				goto('/camp');
			}, 2500)
		} else if ($scope.player.checkUnconcious()){
			generateMessage({text: 'You have been knocked unconcious. Somehow you are back in camp.'});
			clearButtons();
			clearCurrentRoom();
			$timeout(function(){
				lockout.maze = false;
				lockout.travel = false;
				mapService.mapObj = {};
				playerService.player.loseLevel();
				timeService.incrementHours(4);
				goto('/camp');
			}, 2500)
		} else if ($scope.player.checkSway()){
			generateMessage({text: 'You have been swayed by your opponent. You wake up in camp later. You are slightly sore.'});
			clearButtons();
			clearCurrentRoom();
			$timeout(function(){
				lockout.maze = false;
				lockout.travel = false;
				mapService.mapObj = null;
				playerService.player.loseLevel();
				timeService.incrementHours(4);
				goto('/camp');
			}, 2500)
		}
		
		
		
		$scope.combat.nextTurn();
		$scope.turn = $scope.combat.currentTurn;
	}
	
	/*
	* 
	*  Combat Actions
	*
	*/
	
	function flee(){
		var a = $scope.player.attemptFlee($scope.enemy);
		if (a){
			generateMessage({text: 'You have fled battle'});
			lockout.maze = false;
			handleRoom();
			clearCurrentRoom();
		} else {
			generateMessage({text: 'You failed to flee the battle'});
			handleCombat();
		}
	}
	
	function attack(){
		var a = $scope.player.dealDamage($scope.enemy);
		$scope.enemy.takeDamage(a.dmg);
		generateMessage({text: 'You swing wildly '+ a.times +' times, dealing '+a.dmg+' damage to the '+$scope.enemy.race.name+' [ '+convertHealth($scope.combat.enemy.health, $scope.combat.enemy.maxHealth)+' ]'});
		handleCombat();
	}
	
	function sway(){
		var a = $scope.enemy.takeSway($scope.player.dealSway());
		generateMessage({text: 'You speak silvered words. Your enemy has been partially swayed. [ '+ convertHealth(a, $scope.enemy.resolution)+' ]'});
		handleCombat();
	}
	
	function club(){
		if ($scope.enemy.attemptFlee($scope.player)){
			var a = $scope.enemy.takeBlunt($scope.player.dealBlunt());
			generateMessage({text: 'You club the head of your enemy. [ '+ convertHealth(a, 100)+' ]'});
		} else {
			generateMessage({text: 'You missed the enemies head!'});
		}
		handleCombat();
	}
	
	//After Combat Actions
	function arrest(){
		if(playerService.player.keyItems.indexOf('Magic Manacles') != -1){
			generateMessage({text: 'You arrest the creature.'});
		} else {
			generateMessage({text: 'You lack a key item to complete this action'});
		}
		lockout.maze = false;
		playerService.player.changeLawful(2);
		handleRoom();
		clearCurrentRoom();
		timeService.incrementMinutes(5);
	}
	function coup(){
		generateMessage({text: "You murder the helpless creature heartlessly."});
		lockout.maze = false;
		playerService.player.changeGood(-2);
		handleRoom();
		clearCurrentRoom();
		timeService.incrementMinutes(5);
	}
	function leave(){
		generateMessage({text: "You mercifully leave the "+$scope.enemy.race.name+" unconcious but still alive."});
		lockout.maze = false;
		playerService.player.changeGood(2);
		handleRoom();
		clearCurrentRoom();
		timeService.incrementMinutes(5);
	}
	function pickpocket(){
		var gold = Math.floor($scope.enemy.level * .25)
		generateMessage({text: "You rifle through the pockets of the unconcious "+$scope.enemy.race.name+". You find "+gold+" gold."});
		lockout.maze = false;
		playerService.player.changeLawful(-2);
		playerService.player.gold += gold;
		handleRoom();
		clearCurrentRoom();
		timeService.incrementMinutes(5);
	}
	
	//Enemy Actions
	function handleEnemy(a){
		switch(a){
			case 'atk':
				enemyAttack();
				break;
			case 'swy':
				enemySway();
				break;
			case 'club':
				enemyClub();
				break;
			default:
				enemyAttack();
				break;
		}
	}
	
	function enemyAttack(){
		var a = $scope.enemy.dealDamage($scope.player);
		$scope.player.takeDamage(a.dmg);
		generateMessage({text: $scope.enemy.name+' swings '+ a.times+' times and deals '+a.dmg+' damage to '+$scope.player.name});
		desc.enemHealth();
		handleCombat();
	}
	
	function enemySway(){
		var a = $scope.player.takeSway($scope.enemy.dealSway());
		generateMessage({text: 'You have been partially swayed. [ '+ convertHealth(a, $scope.player.resolution)+' ]'});
		handleCombat();
	}
	
	function enemyClub(){
		if ($scope.player.attemptFlee($scope.enemy)){
			var a = $scope.player.takeBlunt($scope.enemy.dealBlunt());
			generateMessage({text: 'You have been clubbed. [ '+ convertHealth(a, 100)+' ]'});
		} else {
			generateMessage({text: 'The enemy missed your head!'});
		}
		
		handleCombat();
	}
	
	lockout.travel = true;
	lockout.maze = false;
	
	var goto = function (path){
		$location.path(path);
	};
	
});
