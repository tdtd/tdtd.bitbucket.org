app.controller('CreateCtrl', function($scope, playerService, buttonService, $location){
	var currentview = 'introtext'
	
	$scope.name = 'Create Controller';
	$scope.player = playerService.player;
	$scope.jobs = jobs;
	$scope.races = races;
	
	$scope.job = 'Warrior';
	$scope.race = 'Human';
	
	
	/*
	*
	*  View
	*
	*/
	$scope.curView = function(loc){
		return (loc == currentview);
	}
	
	$scope.switchView = function(loc){
		currentview = loc;
	}
	
	
	
	$scope.raceStats = function(){
		return playerService.player.race;
	}
	$scope.jobStats = function(){
		return playerService.player.job;
	}
	
	$scope.stats = function(){
		var a = playerService.player.race.stats;
		var b = playerService.player.job.stats;
		var c = {};
		Object.keys(a).forEach(function(key){
			c[key] = a[key] + b[key];
		})
		 return c;
	}
	
	$scope.updateDescription = function(){
		$scope.height = playerService.player.race.height;
		$scope.weight = playerService.player.race.weight;
	}
	
	$scope.finalize = function(){		
		playerService.player.good = $scope.jobStats().alignment.good;
		playerService.player.lawful = $scope.jobStats().alignment.lawful;
		
		playerService.player.created = true;
		playerService.player.playerCharacter = true;
		playerService.player.updateAll();
		playerService.player.sanityCheck();
		$scope.goto('/camp');
	}
	
	// Beautify Alignment Stats
	$scope.alignment = function(a,b){
		var a = align(a, ['Chaotic', 'Neutral', 'Lawful']);
		var b = align(b, ['Evil', 'Neutral', 'Good']);	
		if (a == b){
			a = 'True';
		}
		return a+' '+b;
	}
	
	/*
	*
	* Character Customization Options
	*
	*/
	$scope.hair = hair;
	
	$scope.eyes = eyes;
	
	$scope.skin = skin;
	
	$scope.gender = gender;
	
	$scope.createPlayer = {
		height: 0,
		weight: 0,
	};
	
	$scope.addBlem = function(key, ind){
		var a = $scope.blemishes[key].splice(ind, 1).join('');
		$scope.player.blemishes[key].push(a);
	};
	
	$scope.remBlem = function(key, ind){
		var a = $scope.player.blemishes[key].splice(ind, 1).join('');
		$scope.blemishes[key].push(a);
	};
	
	$scope.blemishes = blemishes;
	
	// ----- Basic Nav Functions
	$scope.getPath = function(){
		$scope.curPath = $location.path();
	};
	
	$scope.goto = function (path){
		$location.path(path);
	};
	
	
	// -------------
	function forceOut(){
		if (playerService.player.created){
			$scope.goto('/camp');
		}
	}
	
	forceOut();
});