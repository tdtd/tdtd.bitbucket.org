app.controller('TownCtrl', function($scope, playerService, buttonService, mapService, townService, timeService, lockout){
	var currentview = 'square';
	
	$scope.player = playerService.player;
	$scope.level = 10;
	$scope.item = ranItem(10);
	$scope.inventory = function(){
		return playerService.player.inventory;
	}
	$scope.prisonAction;
	
	/*
	*
	*  Handle Town Movement
	*
	*/
	$scope.curView = function(loc){
		return (loc == currentview);
	}
	
	$scope.switchView = function(loc){
		resetStates();
		currentview = loc;
	}
	
	function resetStates (){
		$scope.prisonAction = null;
		$scope.allowManacles = false;
	}
	/*
	*
	*   Handle General Store 
	*
	*/
	$scope.generalStore = townService.generalStore;
	
	$scope.inventory = function(){
		return playerService.player.inventory;
	}
	
	$scope.newItem = function(){
		$scope.item = ranItem($scope.level);
	}
	
	$scope.addToInventory = function(){
		playerService.player.addItemToInventory($scope.item);
	}
	
	$scope.upgradeShop = function(){
		if (townService.generalStore.upgrade(176).worked){
			
		} else {
			
		}
	}
	
	$scope.price = function(g){
		return Math.floor(g*townService.generalStore.rebuy);
	}
	
	$scope.sellItem = function(index){
		var item = playerService.player.inventory[index];
		
		if (townService.generalStore.removeGold($scope.price(item.price)).worked){
			var a = playerService.player.removeItemFromInventory(index);
			playerService.player.gold += $scope.price(a.price);
			townService.generalStore.shopInventory.push(a);
		} else {
			console.log('Shop Keeper Doesnt have enuff dosh');
		}
	}
	
	$scope.buyItem = function(index){
		var item = townService.generalStore.shopInventory[index];
		if (playerService.player.removeGold(item.price).worked){
			var a = townService.generalStore.shopInventory.splice(index, 1);
			townService.generalStore.gold += item.price;
			playerService.player.inventory.push(a[0]);
		} else {
			console.log('failed to buy');
		}
	}
	
	/*
	*
	*  Handle Prison
	*
	*/
	$scope.allowManacles = false;
	$scope.approachSheriff = function(){
		$scope.prisonAction = 'You approach the sheriff and make small talk. It feels like the man is staring right through you, as if he sees your inner nature. '
		if (playerService.player.lawful > .66 && playerService.player.keyItems.indexOf('Magic Manacles') == -1) {
			$scope.prisonAction += '"You are a man of the law, I can see it in your eyes." the tired sheriff says gruffly. "I could always use a deputy. If you are up for the job, you can take handcuffs. Anyone you arrest will be telepported to jail and you will receive a bounty for it.';
			$scope.allowManacles = true;
		} else {
			$scope.prisonAction += 'The sheriff talks a bit more and seems to get what he wants.'
		}
	}
	$scope.checkCells = function(){
		if (timeService.daysElapsed > 2){
			$scope.prisonAction = 'The cells are empty.'
		} else {
			$scope.prisonAction = 'There is a desheveled man sitting in the corner. He does not answer your call.'
		}
	}
	
	$scope.getManacles = function(){
		if (playerService.player.keyItems.indexOf('Magic Manacles') == -1){
			playerService.player.keyItems.push('Magic Manacles');
		}
		$scope.allowManacles = false;
	}
	
	mapService.mapObj = null;
	mapService.minimap = null;
});