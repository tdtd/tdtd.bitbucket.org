app.controller('CampCtrl', function($scope, playerService, buttonService, mapService, timeService, lockout){
	var statup = false;
	$scope.map = mapService;
	$scope.player = playerService.player;
	$scope.saves = [];
	$scope.inventory = function(){
		return playerService.player.inventory;
	}
	$scope.equipment = function(){
		return playerService.player.equipment;
	}
	
	$scope.mirror = function(){
		alert("In the mirror you see "+$scope.player.name+" the "+$scope.player.race+" "+ $scope.player.class);
	}
	
	$scope.lock = function(){
		lockout.travel = !lockout.travel;
	}
	
	/*
	*
	*  Camp Sites
	*
	*/
	$scope.camp = new Camp();
	
	/*
	*
	* Player Inventory and Chest Handling
	*
	*/
	$scope.equip = function(item, index){
		playerService.player.equipFromInventory(item.slot, index);
	}
	
	$scope.unequip = function(key){
		playerService.player.unequip(key);
	}
	
	/*
	*
	* rest
	*
	*/
	
	$scope.rest = function(){
		playerService.player.rest();
		if (playerService.player.statPoints > 0){
			statup = true;
		}
		timeService.incrementHours(8);
	}
	
	$scope.statInc = function(){
		return (statup && playerService.player.statPoints > 0);
	}
	$scope.incStat = function(stat){
		playerService.player.statUp(stat);
	}
	
	
	
	/*
	*
	*Save Functions
	*
	*/
	$scope.saveGame = function(num){
		localStorage.setItem('player'+num, JSON.stringify(playerService.player));
		localStorage.setItem('player'+num+'time', JSON.stringify(timeService));
		getSaves();
	}
	
	$scope.loadGame = function(num){
		playerService.player.inventory = [];
		var a = JSON.parse(localStorage.getItem('player'+num));
		var b = JSON.parse(localStorage.getItem('player'+num+'time')); 
		_.merge(playerService.player, a);
		_.merge(timeService, b);
	}
	
	function getSaves(){
		$scope.saves = [];
		for (var i = 1; i <= 5; i++){
			var a = JSON.parse(localStorage.getItem('player'+i));
			if (a == null){
				a = {name:'Empty', level:'0'}
			}
			$scope.saves.push(a);
		}	
	}
	
	getSaves();
	mapService.mapObj = null;
	mapService.minimap = null;
});