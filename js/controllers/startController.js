app.controller('StartCtrl', function($scope, $location, patch, playerService, timeService){
	$scope.name = 'Start Controller';
	$scope.patches = patch;
	$scope.saves = [];
	
	$scope.newChar = function(){
		playerService.player = new Creature({});
	}
	
	$scope.load = function(num){
		var a = JSON.parse(localStorage.getItem('player'+num));
		var b = JSON.parse(localStorage.getItem('player'+num+'time'));
		playerService.player = new Creature({});
		_.merge(playerService.player, a);
		_.merge(timeService, b)
		$scope.goto('/camp');
	}
	
	$scope.getPath = function(){
		$scope.curPath = $location.path();
	};
	
	$scope.goto = function (path){
		$location.path(path);
	};
	
	(function(){
		for (var i = 1; i <= 5; i++){
			var a = JSON.parse(localStorage.getItem('player'+i));
			if (a == null){
				a = {name:'Empty', level:'0'}
			}
			$scope.saves.push(a);
		}	
	})();
	
});