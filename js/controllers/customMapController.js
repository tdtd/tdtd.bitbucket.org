app.controller('CustomMapCtrl', function($scope, mapService){
	var mapObj = new Map(15,15);
	$scope.brush = {
		active: false,
		type: 'wall'
	};
	
	$scope.mapObj = function(){
		return mapObj;
	}
	
	genMap = function(){
		mapObj = new Map(15,15);
		mapObj.fillMap(); // 27/27
	}

	$scope.change = function(item){
		var x = item.x,
				y = item.y;
		if($scope.brush.active){
			mapObj.map[x][y].room = $scope.brush.type;
			mapObj.refreshStates();
		}
		console.log(mapObj.map[x][y]);
	}
	
	genMap();
});