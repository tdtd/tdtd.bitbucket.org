var app = angular.module('app', ['ngSanitize', 'ngRoute', 'ngAnimate', 'ui.bootstrap'])
.config(['$routeProvider', '$locationProvider',
  function($routeProvider, $locationProvider) {
    $routeProvider
			.when('/camp', {
				templateUrl: '/view/campView.html',
        controller: 'CampCtrl',
        controllerAs: ''
      })
      .when('/create', {
        templateUrl: '/view/createView.html',
        controller: 'CreateCtrl',
        controllerAs: ''
      })
			.when('/maze', {
        templateUrl: '/view/mazeView.html',
        controller: 'MazeCtrl',
        controllerAs: ''
      })
			.when('/town', {
        templateUrl: '/view/townView.html',
        controller: 'TownCtrl',
        controllerAs: ''
      })
			.when('/cusMap', {
        templateUrl: '/view/customMapView.html',
        controller: 'CustomMapCtrl',
        controllerAs: ''
      })
			.when('/cusNPC', {
        templateUrl: '/view/customNPCView.html',
        controller: 'CustomNPCCtrl',
        controllerAs: ''
      })
			.when('/', {
        templateUrl: '/view/startView.html',
        controller: 'StartCtrl',
        controllerAs: ''
      })
			.when('',{
				redirectTo: '/'
			})
			.otherwise({
        redirectTo: '/'
      });

	}])
.run(function($rootScope, $templateCache) {
   $rootScope.$on('$viewContentLoaded', function() {
		 $templateCache.removeAll();
   });
});

app.directive('keypressEvents',
function ($document, $rootScope) {
    return {
        restrict: 'A',
        link: function () {
            $document.bind('keypress', function (e) {
                $rootScope.$broadcast('keypress', e);
            });
        }
    }
});

app.factory('lockout', function(){
	var lock = {
		travel: false,
		maze: true
	}
	return lock;
});

app.controller('MainCtrl', function($scope, $location, $rootScope, playerService, buttonService, mapService, timeService, lockout){
	$scope.curLoc = [0,0];
	var classHold = 's';
	
	
	$scope.player = function(){
		return playerService.player;
	} 
	$scope.buttons = buttonService;
	$scope.map = mapService;
	$scope.name = 'main'
	
	$scope.locked = lockout;
	
	$scope.localArea = [[{}]];
	
	$scope.inc = function(){
		$scope.curLoc[0]++;
	}
	
	$scope.$watchCollection('curLoc', function(newVal, oldVal){
		if (mapService.mapObj){
			mapService.minimap = mapService.mapObj.localArea(newVal[1], newVal[0]);
		}
	});
	
	$scope.genMap = function(){
		mapService.genMap(varyLvl(playerService.player.level, 5, 5));
		$scope.goto('/maze')
		$scope.curLoc = mapService.mapObj.entrance;
		classHold = 's'
		$scope.move([0,0])
	}
	
	// Beautify Alignment Stats
	$scope.alignment = function(a,b){
		var a = align(a, ['Chaotic', 'Neutral', 'Lawful']);
		var b = align(b, ['Evil', 'Neutral', 'Good']);	
		if (a == b){
			a = 'True';
		}
		return a+' '+b;
	}
	
	
	/*
	*
	*   Handle Movement Around Map
	*
	*/
	$scope.checkWall = function(arr){
		//console.log(arr);
		if (typeof arr !== 'undefined' && arr.collide == 'false' ){
			return true;
		}
		return false;
	}
	
	$scope.move = function(deltArr){
		var a = $scope.curLoc;
		
		mapService.mapObj.map[a[0]][a[1]].room = classHold;
		
		$scope.curLoc[0] += deltArr[0];
		$scope.curLoc[1] += deltArr[1];
		revealRooms($scope.curLoc);
		classHold = mapService.mapObj.map[$scope.curLoc[0]][$scope.curLoc[1]].room;
		
		mapService.mapObj.map[$scope.curLoc[0]][$scope.curLoc[1]].class = classHold+' p';
		mapService.currentLoc = $scope.curLoc;
		
		$scope.$broadcast('moved', {
			m: 'moved' // send whatever you want
		});
	}
	
	function revealRooms(arr){
		var delts = [[0,1], [0,-1], [1,0], [-1,0], [-1, -1],[-1, 1],[1, -1],[1, 1],[0, -2],[-2, 0],[0, 2],[2, 0],];
		var m = mapService.mapObj.map;
		delts.forEach(function(delt){
			var a = arr[0] + delt[0],
					b = arr[1] + delt[1];
			
			if (a >= 0 && b >= 0){
				if (m[a][b].room != 'w'){
					mapService.mapObj.map[a][b].class = m[a][b].room;
				}
			}
		});
	}
	
	//Keypress Watcher for Key Controls
	 $rootScope.$on('keypress', function (evt, obj) {
        $scope.$apply(function () {
					var a;
					switch (obj.keyCode){				
						case 119: //W
							a = [-1, 0];
							break;
						case 97: //A
							a = [0, -1];
							break;
						case 115: //S
							a = [1, 0];
							break;
						case 100: //D
							a = [0, 1];
							break;
						default:
							return;
							break;
						}
					if (!lockout.maze && $scope.map.mapObj){
						if ($scope.checkWall($scope.map.mapObj.map[$scope.curLoc[0]+a[0]][$scope.curLoc[1]+a[1]])){
						
							$scope.move(a);
						}
					}
        });
    });
	
	
	/*
	*
	*  Watch Time Service
	*
	*/
	$scope.time = function(){
		return timeService;
	};
	
	// ----- Sidebar Functions
	$scope.healthBar = function(){
		return 'Health: '+ convertHealth($scope.player().health, $scope.player().maxHealth);
	}
	$scope.manaBar = function(){
		return 'Mana: '+ convertHealth($scope.player().mana, $scope.player().maxMana);
	}
	$scope.staminaBar = function(){
		return 'Stamina: '+ convertHealth($scope.player().stamina, $scope.player().maxStamina);
	}
	$scope.experienceBar = function(){
		return 'Experience: '+ convertHealth($scope.player().experience, $scope.player().nextLevel);
	}
	
	
	// ----- Basic Nav Functions
	$scope.getPath = function(){
		$scope.curPath = $location.path();
	};
	
	$scope.goto = function (path){
		$location.path(path);
	};
});



function surrogateCtor() {}
 
function extend(base, sub) {
  // Copy the prototype from the base to setup inheritance
  surrogateCtor.prototype = base.prototype;
  // Tricky huh?
  sub.prototype = new surrogateCtor();
  // Remember the constructor property was set wrong, let's fix it
  sub.prototype.constructor = sub;
}

/*
*
* Random Functions
*
*/

function align(a, b){
	if (a <= .33){
		return b[0];
	} else if (a > .33 && a <= .66){
		return b[1];
	} else {
		return b[2];
	}
};

//RNG function
function ran(min, max){
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function zero (a){
 return (a < 0) ? 0 : a;
}


//Combining Stat Function
function calcSecondary(a,b){
	return Math.floor((a + (b*.5)));
}

//Calculate damage w/o going below zero
function calcToZero(a, b){
	var c = a - b,
			o = {}
	if (c <= 0){
		o.remain = 0;
		o.overkill = c;
		return o;
	} else {
		o.remain = c;
		return o;
	}
}

//Convert Numeral health to a text health Bar
function convertHealth(val, max){
	var percent = val/max;
	var total = Math.floor((percent*10));
	var arr = [];
	for (var i = 0; i < 10; i++){
		if (i < total){
			arr.push('■');
		} else {
			arr.push('□');
		}
	}
	arr.push(' '+val+' | '+max);
	return arr.join('');
}

//Vary Number between the High and Low
var varyLvl = function(lvl, high, low){
    low -= 1;
    var variance = (Math.ceil(Math.random() * (high - low) + low));
    var finalNum = 0;
    if ((lvl + variance) < 1){
     finalNum = 1;   
    } else {
     finalNum = lvl + variance;   
    }
    return finalNum;
};

var randomProperty = function (obj) {
	var keys = Object.keys(obj)
	return keys[ keys.length * Math.random() << 0];
};