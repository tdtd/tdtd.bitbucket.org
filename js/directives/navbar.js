app.directive('navbar', function() { 
  return { 
    restrict: 'E', 
    templateUrl: './js/directives/navbar.html' 
  }; 
});
