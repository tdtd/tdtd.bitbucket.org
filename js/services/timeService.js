app.factory('timeService', function (playerService, townService) {
	function Time(){
		this.returned = 0;
		this.daysElapsed = 1;
		this.daysEnd = 7;
		this.hours = 0;
		this.minutes = 0;

	}

	Time.prototype.endCheck = function(){
		if (this.daysElapsed > this.daysEnd){
			this.returned++;
			this.reset();
			return true;
		}
		return false;
	}
	
	Time.prototype.forceEnd = function(){
		this.returned++;
		this.reset();
	}
	
	Time.prototype.incrementMinutes = function (time){
		var a = this.minutes + time,
				b = Math.floor(a/60);
		if (b){
			this.incrementHours(b);
			this.minutes = a % 60;
		} else {
			this.minutes = a;
		}
	}
	Time.prototype.incrementHours = function(time){
		var a = this.hours + time,
				b = Math.floor(a/24);
		if (b){
			this.incrementDays(b);
			this.hours = a % 24;
		} else {
			this.hours = a;
		}
	}

	Time.prototype.incrementDays = function(time){
		this.daysElapsed += time;
		var day = 'day'+this.daysElapsed;
		if (this.endCheck()){
			
		} else {
			this[day]();
		}
		
		
	}
	
	
	Time.prototype.daily = function(){
		townService.generalStore.setNewInventory(playerService.player.level);
		townService.generalStore.refreshGold();
	}
	
	Time.prototype.day0 = function(){
		this.daily();
	}
	Time.prototype.day1 = function(){
		this.daily();
	}
	Time.prototype.day2 = function(){
		this.daily();
	}
	Time.prototype.day3 = function(){
		this.daily();
	}
	Time.prototype.day4 = function(){
		this.daily();
	}
	Time.prototype.day5 = function(){
		this.daily();
	}
	Time.prototype.day6 = function(){
		this.daily();
	}
	Time.prototype.day7 = function(){
		this.daily();
	}


	Time.prototype.reset = function(){
		this.daysElapsed = 1;
		this.minutes = 0;
		this.hours = 0;
		townService.generalStore.shopUpgradeLevel = 0;
	}
	
	
	
	
	
	var timeServ = new Time();
 
	return timeServ;
});