app.factory('mapService', function () {
	function MapServ(){
		this.mapObj;
		this.genMap = function(lvl){
			this.mapObj = new Map(30,30, lvl);
			this.mapObj.fillMap(); // 27/27
		};
		this.miniMap;
		this.currentLoc = [0,0];
	};
	
	var mapObj = new MapServ();
 
	return mapObj;
});


function useCustomMap(m){
	var a = new Map(m.height, m.width);
	for (var i = 0; i < m.height; i++){
		for (var j = 0; j < m.width; j++){
			_.merge(a.map[i][j], m.map[i][j]);
			console.log(a.map[i][j]);
		}
	}
	a.entrance = m.entrance;
	a.exit = m.exit;
	return a;
}