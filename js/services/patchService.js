app.factory('patch', function () {
	var patches = [
		{
			patchNo: '0.0.4',
			notes: ['Expanded upon character generation', 'Changed the way combat works, from a page based on the room to a text line based system', 'Swapped from bootstrap progress bars to custom text bars to fit more in line with current dungeon system', 'Added combat with five options (Inspect, Attack, Club, Persuade and Flee)'],
			date: new Date("Feb 15, 2016")
		}
	];
 
	return patches;
});
